from django.shortcuts import render, redirect
from django.http import Http404, JsonResponse,HttpResponse
from django.core.exceptions import SuspiciousOperation
from django.views.decorators.csrf import csrf_exempt
import urllib
import json
from datetime import datetime, timedelta, timezone
from .models import Student

WEBHOOK_URL = 'https://hooks.slack.com/services/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'
VERIFICATION_TOKEN = 'asUYX75kGJ1K45O17gDtqCC5'
NUM_TEAMS = 7

def index(request):
    return HttpResponse("Hello")
def send(request):
    if request.method == 'POST':
        data = {
            'text': request.POST['message']
        }
        postMessage(data)

    return redirect(index)

@csrf_exempt
def timeview(request):
    if request.method != 'POST':
        return JsonResponse({})
    
    if request.POST.get('token') != VERIFICATION_TOKEN:
        raise SuspiciousOperation('Invalid request.')

    user_id = request.POST['user_id']
    time = gettime() 
    result = {
        'text': '<@{0}> {1}'.format(user_id, time.strftime("%Y年%m月%d日 %H:%M")),
        'response_type':'in_channel'
    }
    
    return JsonResponse(result)


def postMessage(data):
    headers = {
        'Content-Type': 'application/json',
    }
    req = urllib.request.Request(WEBHOOK_URL, json.dumps(data).encode(), headers)
    with urllib.request.urlopen(req) as res:
        body = res.read()


def gettime():
    JST = timezone(timedelta(hours=+9), 'JST')
    return datetime.now(JST)